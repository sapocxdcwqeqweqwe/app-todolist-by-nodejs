import React, { useEffect, useState } from "react";
import { Card, Row, Col, Button } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import Menu from "./Menu";

const EditDetail = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [toDoListId, setTodoDetail] = useState();

  
  const [detail, setDetail] = useState({
    name: "",
    timeAt: "",
    timeTo: "",
    description: "",
    isDone: false,
  });

  useEffect(() => {
    fetch(`http://localhost:9999/to_do_list_detail/${id}`)
      .then((res) => res.json())
      .then((data) => {
        setTodoDetail(data.toDoListId);
        setDetail(data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [id]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setDetail((prevDetail) => ({
      ...prevDetail,
      [name]: value,
    }));
  };



  const handleSubmit = (e) => {
    e.preventDefault();
    fetch(`http://localhost:9999/to_do_list_detail/${id}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(detail),
    })
      .then((response) => response.json())
      .then(() => {
        alert("Detail updated successfully");
        navigate("/todo/detail/" + toDoListId);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <Menu/>
      <Row className="justify-content-center" style={{height:"100vh", backgroundImage: "linear-gradient(#D0DAE0, #E3EBED)"}}>
        <Col xs={4}>
      <Card style={{padding:"10px", marginTop: "10vh", borderRadius: "2vh"}}>
        <Card.Title><h2 style={{textAlign: "center"}}>Edit Detail</h2></Card.Title>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Name:</label>
          <input
            type="text"
            name="name"
            value={detail.name}
            onChange={handleChange}
            style={{width:"100%", padding: "5px"}}
          />
        </div>
        <div>
          <label>Time At:</label>
          <input
            type="text"
            name="timeAt"
            value={detail.timeAt}
            onChange={handleChange}
            style={{width:"100%", padding: "5px"}}
          />
        </div>
        <div>
          <label>Time To:</label>
          <input
            type="text"
            name="timeTo"
            value={detail.timeTo}
            onChange={handleChange}
            style={{width:"100%", padding: "5px"}}
          />
        </div>
        <div>
          <label>Description:</label>
          <textarea
            name="description"
            rows="4"
            value={detail.description}
            onChange={handleChange}
            style={{width:"100%", padding: "5px"}}
          ></textarea>
        </div>
        <div>
          <label>Status: &nbsp;</label>
          <input
            type="checkbox"
            name="isDone"
            checked={detail.isDone}
            onChange={handleChange}
          /> Done
        </div>
        <Button className="btn btn-success" type="submit">Update</Button> &nbsp;
        <Link to={"/todo/detail/" + toDoListId}><Button className="btn btn-primary" type="submit">Back</Button></Link>
      </form>
      </Card>
      </Col>
      </Row>
    </div>
  );
};

export default EditDetail;
